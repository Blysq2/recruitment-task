let gulp = require('gulp'),
    watch = require('gulp-watch'),
    browserSync = require('browser-sync');

gulp.task('watch', function() {

    browserSync.init({
        notify: false,
        server: {
            baseDir: "./"
        }
    });

    watch('./style/**/*.scss', function() {
        gulp.start('cssInject');
    });

    watch('./src/**/*.js', function() {
        gulp.start('scriptsRefresh');
    })
});

gulp.task('cssInject', ['sass'], function() {
    return gulp.src('./temp/styles/style.css')
        .pipe(browserSync.stream());
});

gulp.task('scriptsRefresh', ['scripts'], function() {
    browserSync.reload();
});