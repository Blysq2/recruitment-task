import * as types from '../actions/actionTypes';

export default function(state = {}, action) {
    switch (action.type) {
        case types.FETCH_PROFILE:
            return action.profile;
        case types.LIKE_PROFILE:
            return {
                ...state,
                details: {
                    ...state.details,
                    isLiked: true,
                    likes: ++state.details.likes
                }
            };
        case types.UNLIKE_PROFILE:
            return {
                ...state,
                details: {
                    ...state.details,
                    isLiked: false,
                    likes: --state.details.likes
                }
            };
        case types.FOLLOW_PROFILE:
            return {
                ...state,
                details: {
                    ...state.details,
                    isFollowed: true,
                    followers: ++state.details.followers
                }
            };
        case types.UNFOLLOW_PROFILE:
            return {
                ...state,
                details: {
                    ...state.details,
                    isFollowed: false,
                    followers: --state.details.followers
                }
            };
        case types.ADD_COMMENT:
            return {
                ...state,
                comments: [...state.comments, action.comment]
            };
        default:
            return state;
    }
}