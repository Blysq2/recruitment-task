import { renderComponent , expect } from '../tools/testHelper';
import Widget from '../../components/Widget';

describe('Widget' , () => {
    let component;
    beforeEach(() => {
        component = renderComponent(Widget,{
            title: "title1",
            counter: 12
        });
    });

    it('has the correct class', () => {
        expect(component).to.have.class('widgets__widget');
    });

    it('has the correct paragraphs classes', () => {
        expect(component.find('.widgets__counter')).to.exist;
        expect(component.find('.widgets__title')).to.exist;
    });

    it('has the correct values', () => {
        expect(component).to.contain("title1");
        expect(component).to.contain(12);
    });
});