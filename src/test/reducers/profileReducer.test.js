import reducer from '../../reducers/profileReducer';
import * as types from '../../actions/actionTypes';
import expect from 'expect';

const INITIAL_STATE = { details: [], comments: null };

describe('Profile reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual({ });
    });

    it('should handle FETCH_PROFILE', () => {
        const profile = {
            details: {
                _id: 1,
                fullname: "Harvey Specter",
                address: "New York, USA",
                profilePhoto: "public/images/profile-photo.png",
                profileUrl: "http://localhost/profile/harvey-spacter",
                likes: 121,
                following: 723,
                followers: 4432,
                isLiked: false,
                isFollowed: false
            },
            comments: []
        };
        expect(reducer(INITIAL_STATE, {
                type: types.FETCH_PROFILE,
                profile: profile
            })
        ).toEqual(profile);
    });
});