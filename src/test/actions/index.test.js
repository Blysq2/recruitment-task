import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import expect from 'expect';
import * as actions from '../../actions/index';
import * as types from '../../actions/actionTypes';

const mockStore = configureMockStore([thunk]);

describe('Profile actions', () => {
    it('should create a FETCH_PROFILE action', () => {
        let profile ={
            details: {
                _id: 1,
                fullname: "Harvey Specter",
                address: "New York, USA",
                profilePhoto: "public/images/profile-photo.png",
                profileUrl: "http://localhost/profile/harvey-spacter",
                likes: 121,
                following: 723,
                followers: 4432,
                isLiked: false,
                isFollowed: false
            },
            comments: [
                {
                    _id: 22,
                    profilePhoto: "public/images/profile-photo.png",
                    fullname: "Mike Ross",
                    daysAgo: 1,
                    content: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui non feils. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula."
                },
                {
                    _id: 23,
                    profilePhoto: "public/images/profile-photo.png",
                    fullname: "Rachel Zain",
                    daysAgo: 7,
                    content: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui non feils. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula."
                },
                {
                    _id: 24,
                    profilePhoto: "public/images/profile-photo.png",
                    fullname: "Louis Litt",
                    daysAgo: 5,
                    content: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui non feils. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula."
                },
            ]
        };

        const expectedActions = [{
            type: types.FETCH_PROFILE,
            profile: profile
        }];

        const store = mockStore({ details: {}, comments: [] });

        return store.dispatch(actions.fetchProfile()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('should create a LIKE_PROFILE action', () => {
        const expectedActions = [{
            type: types.LIKE_PROFILE
        }];

        const store = mockStore({ details: {}, comments: [] });

        return store.dispatch(actions.likeProfile()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('should create a UNLIKE_PROFILE action', () => {
        const expectedActions = [{
            type: types.UNLIKE_PROFILE
        }];

        const store = mockStore({ details: {}, comments: [] });

        return store.dispatch(actions.unlikeProfile()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('should create a FOLLOW_PROFILE action', () => {
        const expectedActions = [{
            type: types.FOLLOW_PROFILE
        }];

        const store = mockStore({ details: {}, comments: [] });

        return store.dispatch(actions.followProfile()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });

    it('should create a UNFOLLOW_PROFILE action', () => {
        const expectedActions = [{
            type: types.UNFOLLOW_PROFILE
        }];

        const store = mockStore({ details: {}, comments: [] });

        return store.dispatch(actions.unfollowProfile()).then(() => {
            expect(store.getActions()).toEqual(expectedActions);
        });
    });
});