const profile = {
    details: {
        _id: 1,
        fullname: "Harvey Specter",
        address: "New York, USA",
        profilePhoto: "public/images/profile-photo.png",
        profileUrl: "http://localhost/profile/harvey-spacter",
        likes: 121,
        following: 723,
        followers: 4432,
        isLiked: false,
        isFollowed: false
    },
    comments: [
        {
            _id: 22,
            profilePhoto: "public/images/profile-photo.png",
            fullname: "Mike Ross",
            daysAgo: 1,
            content: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui non feils. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula."
        },
        {
            _id: 23,
            profilePhoto: "public/images/profile-photo.png",
            fullname: "Rachel Zain",
            daysAgo: 7,
            content: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui non feils. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula."
        },
        {
            _id: 24,
            profilePhoto: "public/images/profile-photo.png",
            fullname: "Louis Litt",
            daysAgo: 5,
            content: "Lorem ipsum dolor sit amet enim. Etiam ullamcorper. Suspendisse a pellentesque dui non feils. Maecenas malesuada elit lectus felis, malesuada ultricies. Curabitur et ligula."
        },
    ]
};

const generateId = () => {
    return Math.floor((Math.random() * 100000) + 1);
};
class ProfileApi {
    static getProfile() {
        return new Promise((resolve, reject) => {
            resolve(Object.assign({}, profile));
        });
    }

    static likeProfile() {
        return new Promise((resolve, reject) => {
            resolve("ok");
        });
    }

    static unlikeProfile() {
        return new Promise((resolve, reject) => {
            resolve("ok");
        });
    }

    static followProfile() {
        return new Promise((resolve, reject) => {
            resolve("ok");
        });
    }

    static unfollowProfile() {
        return new Promise((resolve, reject) => {
            resolve("ok");
        });
    }

    static addComment(content) {
        return new Promise((resolve, reject) => {
            resolve({
                _id: generateId(),
                profilePhoto: "public/images/profile-photo.png",
                fullname: "New comment",
                daysAgo: 0,
                content: content
            });
        });
    }
}

export default ProfileApi;