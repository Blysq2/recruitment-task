import React from 'react';
import PropTypes from 'prop-types';

const Widget = ({title, counter}) => {
    return (
        <div className="widgets__widget">
            <p className="widgets__counter">{counter}</p>
            <p className="widgets__title">{title}</p>
        </div>
    );
};

Widget.propTypes = {
    title: PropTypes.string.isRequired,
    counter: PropTypes.number.isRequired
};

export default Widget;