import React, { Component } from 'react';
import PropTypes from 'prop-types';
import CommentsList from './CommentsList';

class Timeline extends Component {
    constructor(props) {
        super(props);

        this.state = {
            newComment: '',
            showComments: true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
        this.toggleShowComments = this.toggleShowComments.bind(this);
    }

    handleChange(e) {
        this.setState({
            newComment: e.target.value,
            showComments: this.state.showComments
        });
    }

    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.props.onCommentAdd(this.state.newComment);
            this.setState({
                newComment: "",
                showComments: this.state.showComments
            });
        }
    }

    toggleShowComments() {
        this.setState({
            newComment: this.state.newComment,
            showComments: !this.state.showComments
        });
    }

    render() {
        let {showComments} = this.state;
        return (
            <div className="timeline">
                <p className="timeline__hide-comments" onClick={this.toggleShowComments}>
                    {showComments ? "Hide" : "Show"} comments({this.props.comments.length})
                </p>
                <CommentsList comments={this.props.comments} showComments={showComments}/>
                <div className="timeline__new-comment">
                    <input type="text" placeholder="Add a comment"
                           value={this.state.newComment}
                           onChange={this.handleChange}
                           onKeyPress={this.handleKeyPress}
                    />
                </div>
            </div>
        );
    }
}

Timeline.propTypes = {
    onCommentAdd: PropTypes.func.isRequired,
    comments: PropTypes.array.isRequired
};

export default Timeline;